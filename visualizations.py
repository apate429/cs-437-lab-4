import boto3
import pandas as pd
from matplotlib import pyplot as plt

# creating IOT Analytics client
client = boto3.client('iotanalytics')

# dataset = "vehicle_0_dataset"
# for the second visualization we used the vehicle 1 dataset
dataset = "vehicle_1_dataset"


dataset_url = client.get_dataset_content(datasetName = dataset)['entries'][0]['dataURI']
data = pd.read_csv(dataset_url)
max_emission = list(data['max_co2'])

# for the second visualization i plotted both the max_emission and the current co2_emissions
co2_emission = list(data['co2'])
times = list(data['time_stamp'])
plt.plot(times, max_emission)
plt.plot(times, co2_emission)
plt.xlabel('timestamp')
plt.ylabel('co2 emission values')
# plt.title('change of max co2 emission val vs. timestamp - vehicle 0')
plt.title('change of max co2 emission val and current co2 val - vehicles 0 and 1')
plt.show()
